#!/usr/bin/env python
import sys
import os
import shutil
import tempfile
import datetime
import glob
import zipfile
import signal
sys.path.append("lib/")

from bottle import route, run, static_file, request, template, response, auth_basic

MC_BASEPATH = "./mc"
PID_FILE = "server.pid"

def check_pass(username, password):
    print username
    if password in ["mc_is_good"]:
        return True
    return False

def kill_server(pid_file=PID_FILE):
    pid_path = os.path.join(MC_BASEPATH, PID_FILE)
    if os.path.isfile(pid_path):
        pid = open(os.path.join(MC_BASEPATH, PID_FILE)).read()
        try:
            os.kill(int(pid), signal.SIGKILL)
        except OSError:
            pass
        return True
    return False

@route('/')
@auth_basic(check_pass)
def index():
    """ the index to list map """
    return template('index')

@route('/delete')
@auth_basic(check_pass)
def delete(world="world"):
    """ delete world folder """
    world_dir = os.path.join(MC_BASEPATH, world)
    if os.path.isdir(world_dir):
        shutil.rmtree(world_dir)
    os.mkdir(world_dir)
    return "World deleted, please restart MC server to generate a new one."

@route('/upload', method='POST')
@auth_basic(check_pass)
def upload(world="world"):
    """ upload and replace world """
    upload = request.files.get('upload')
    name, ext = os.path.splitext(upload.filename)
    if ext not in ('.zip'):
        return 'File extension not allowed.'
    fd, fn = tempfile.mkstemp(prefix=world, suffix=".zip")
    upload.save(fn, overwrite=True)

    zf = zipfile.ZipFile(fn)
    for fn in zf.namelist():
        if not fn.startswith(world + "/"):
            return "%s is not a valid world path"
        if ".." in fn:
            return "bad zip"
    zf.extractall(MC_BASEPATH)

    fn_list_str = "\n".join(zf.namelist())

    response.content_type = 'text/plain; charset=utf8'

    # Kill server to avoid map save.
    kill_server()
    return "Map uploaded, restarting server, list:\n%s" % fn_list_str

@route('/download')
@auth_basic(check_pass)
def download(world="world"):
    """ zip world and serve """
    fd, fn = tempfile.mkstemp(prefix=world)
    # clean up old archives
    for old_file in glob.glob(os.path.join(os.path.basename(fn), world + "*")):
        os.unlink(old_file)
    # make zip
    shutil.make_archive(fn, 'zip', root_dir=MC_BASEPATH, base_dir=world)
    # serve
    now = datetime.datetime.now()
    download_name = "%s_%s.zip" % (world, now.strftime("%y%m%d_%H%M%S"))
    return static_file(fn + ".zip", root="/", download=download_name)

# run(host='localhost', port=2051, debug=True, auto_reload=True)
run(host='0.0.0.0', port=2051)
